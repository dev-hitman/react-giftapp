

export const getGifs = async (category) => {
    const url =
     `https://api.giphy.com/v1/gifs/search?q=${category}&limit=20&api_key=4mVRLXrkvndrmnVV8xBg76Z73Toe8zQn`;
    //llamado al api de gifs
    const resp = await fetch(url);
    const { data } = await resp.json();

    const gifs = data.map((img) => {
      return {
        id: img.id,
        title: img.title,
        url: img.images?.downsized_large.url,
      };
    });
    
    return gifs
    
  };