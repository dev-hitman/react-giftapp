import React, { useState } from "react";

import { AddCategoryComponent } from "./components/AddCategoryComponent";
import { GifColecction } from "./components/GifColecction";

export const GifExpertComponent = () => {
  const [category, setCategory] = useState(["One Push Man"]);

  return (
    <>
      <h2>GifExpertApp</h2>

      <AddCategoryComponent setCategory={setCategory} />

      <hr></hr>

      <ol>
        {category.map((category) => (
          <GifColecction key= {category} category = {category}/>
        ))}
      </ol>
    </>
  );
};
