import React, { useEffect, useState } from "react";
import { getGifs } from "../services/GetGifts";
import { GiftColectionItem } from "./giftColectionItem";

export const GifColecction = ({ category }) => {
  const [images, setimages] = useState([]);

  useEffect(() => {
    getGifs(category).then(img => setimages(img))
  }, [category]);



  return (
     <>
 
      <h3>{category}</h3>
    <div className='card-grid'>
      {images.map((img) => {
      return <GiftColectionItem {...img} key={img.id} />})
      }
    </div>
     
     </> 
  );
};
