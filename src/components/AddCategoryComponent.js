import React, { useState } from 'react'
import PropTypes from 'prop-types';

export const AddCategoryComponent = ({setCategory}) => {

//Hook del input    
const [inputValue, setinputValue] = useState('')

//evento que obtinene los valores cuando se cambia el valor del hook
const handleChangesValue = (event)=>{

    //obtinene y asigna el valor del input al hook
    setinputValue(event.target.value)

}


//funcion que recibe el event del formulario
const handleSubmitForm = (event)=>{

//prevencion al reinicio de la pagina al hacer submit
    event.preventDefault()
//validacion y creacion de categoria
    if(inputValue.trim().length > 1){

        setCategory(cat =>[ inputValue, ...cat,])

        setinputValue('');
    
    }


}

    return (

        <form onSubmit= {handleSubmitForm}>
            <input 
                type= 'text'
                value= {inputValue}
                onChange = {handleChangesValue}
            />

            
        </form>
    )
}

AddCategoryComponent.propTypes={
    setCategory: PropTypes.func.isRequired
}
